package dz_11;

public class NumberUtil {
    public static boolean isSimple(int number) {
//        if (number == 0) {
//            throw new RuntimeException("Зачем был передан ноль???");
//        }


        if (number == 1 || number == 2) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static int biggestDivisor(int num1, int num2) {

        num1 = Math.abs(num1);
        num2 = Math.abs(num2);

        if (num1 == 0 || num2 == 0 || (num1 == 0 && num2 == 0)) {
            throw new RuntimeException("Зачем был передан ноль???");
        }

        int divisor = Math.min(num1, num2);

        if (Math.max(num1, num2) % divisor == 0) {
            return divisor;
        } else {
            divisor = divisor / 2;
        }

        while (divisor > 0) {
            if (num1 % divisor != 0 || num2 % divisor != 0) {
                divisor = divisor - 1;
            } else {
                break;
            }
        }

        return divisor;
    }


}
