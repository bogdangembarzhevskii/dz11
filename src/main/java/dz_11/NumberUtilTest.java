package dz_11;

import dz_11.NumberUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class NumberUtilTest {

    // private final NumberUtil numberUtil = new NumberUtil(); СТАТИЧЕСКИЙ

    //@Test
//    @ParameterizedTest(name = "return <true> on {0}")
//    @ValueSource(ints = {3, 7, 13, 17})
//    @DisplayName("Проверка метода isSimple() на простых числах")
//    void isSimple(int number) {
//        //Assertions.assertEquals(true, NumberUtil.isSimple(number));
//        assertTrue(NumberUtil.isSimple(number));
//    }

    @ParameterizedTest
    @CsvSource(value = {
            "2, 13, 1",
            "0, 13, 169"})
    void maxDivider(int num1, int num2, int value) {
        int result = NumberUtil.biggestDivisor(num1, num2);
        assertEquals(value, result);
    }


}